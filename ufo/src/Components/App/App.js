import React from 'react';
import './App.css';
import DrawerNav from '../DrawerNav/DrawerNav.js';
import Header from '../Header/Header.js';
import SubHeader from '../SubHeader/SubHeader.js';
import Charts from '../Charts/Charts.js';

function App() {
  return (
      <span>
    <div className="App">
      <DrawerNav />

      <div className="rightSide">
          <Header/>  
          <SubHeader/>
          <Charts/>
       </div>
    </div>
      </span>
  );
}

export default App;
    //<img src='/mock.png' className='mock'></img>
