import React, { Component } from 'react';
import { observer , inject } from 'mobx-react';
import Grid from '@material-ui/core/Grid';
import Cities from './Cities';
import Shapes from './Shapes';

import './Charts.scss'

class Charts extends Component {

    constructor(props){
        super(props);
    }
    render() {
           return (

             <Grid container spacing={0} className="">
               <Grid item xs={12} sm={12} md={12} lg={6} className="">
               <Cities/>
               </Grid>
               <Grid item xs={12} sm={12} md={12} lg={6} className="">
               <Shapes/>
               </Grid>
             </Grid>

           );


            }
  }


export default Charts ;

