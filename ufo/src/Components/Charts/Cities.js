import React, { Component } from 'react';
import { observer , inject } from 'mobx-react';
import {
  XYPlot,
  XAxis,
  YAxis,
  VerticalGridLines,
  HorizontalGridLines,
  VerticalBarSeries,
  VerticalBarSeriesCanvas,
  LabelSeries,
    FlexibleWidthXYPlot
} from 'react-vis';
import './Cities.scss';
class Cities extends Component {

    componentWillMount(){
        this.props.UfoCityStore.getCities()
    }
    render(){
        const {cities, inProgress} = this.props.UfoCityStore

        const getChart = ()=> {
            var cityData = []
            if (cities.length > 0){
                cities.forEach( ( city, index ) => {
                    var item = {}
                    item.x = city._id
                    item.y = city.number 
                    cityData.push(item)
                 })
                
                return  (
                    <FlexibleWidthXYPlot xType="ordinal" height={300} xDistance={100}>
                      <VerticalGridLines />
                      <HorizontalGridLines />
                      <XAxis bottom={10} />
                      <YAxis left={10}/>
                      <VerticalBarSeries color='#81D0FD' data={cityData} />
                    </FlexibleWidthXYPlot>
                )
            }
        }

        return (
            <div className='chartItem'>
                <div className='chartText'>CITIES REPORTING THE MOST UFO SIGHTINGS</div>
                {getChart()}
            </div>
        )
    }
}
Cities = inject('UfoCityStore')(observer(Cities ))
export default Cities 
