import React, { Component } from 'react';
import { observer , inject } from 'mobx-react';
import {
  XYPlot,
  XAxis,
  YAxis,
  VerticalGridLines,
  HorizontalGridLines,
  VerticalBarSeries,
  VerticalBarSeriesCanvas,
  LabelSeries,
    FlexibleWidthXYPlot
} from 'react-vis';
class Shapes extends Component {

    componentWillMount(){
        this.props.UfoShapeStore.getShapes()
    }
    render(){
        const {shapes, inProgress} = this.props.UfoShapeStore

        const getChart = ()=> {
            var cityData = []
            if (shapes.length > 0){
                shapes.forEach( ( shape, index ) => {
                    var item = {}
                    item.x = shape._id
                    item.y = shape.number 
                    item.style = {fontSize:2}
                    cityData.push(item)
                 })
                
                return  (
                    <FlexibleWidthXYPlot    xType="ordinal" height={300} xDistance={40}>
                      <XAxis bottom={10} />
                      <YAxis left={25} />
                      <VerticalBarSeries style={{fontSize:10}} margin={{left: 400}}  color='#81D0FD' data={cityData} />
                    </FlexibleWidthXYPlot>
                )
            }
        }

        return (
            <div className='chartItem'>
                <div className='chartText'>MOST COMMONLY REPORTED UFO SHAPES</div>
                {getChart()}
            </div>
        )
    }
}
Shapes = inject('UfoShapeStore')(observer(Shapes))
export default Shapes 
