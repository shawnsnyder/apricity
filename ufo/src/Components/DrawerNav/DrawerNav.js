import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import IconButton from '@material-ui/core/IconButton';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import MenuIcon from '@material-ui/icons/Menu';

import MailIcon from '@material-ui/icons/Mail';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import BarChart from '@material-ui/icons/BarChart';
import ShareOutlined from '@material-ui/icons/ShareOutlined';
import DonutLarge from '@material-ui/icons/DonutLarge';
import FolderOpenOutlined from '@material-ui/icons/FolderOpenOutlined';
import ChromeReaderMode from '@material-ui/icons/ChromeReaderMode';


import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';


import './DrawerNav.scss'

const drawerWidth = 200;

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',

    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
          width: drawerWidth,
          flexShrink: 0,
        },
    },
    appBar: {
        backgroundColor: 'white',
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
          width: `calc(100% - ${drawerWidth}px)`,
        },
        height:'43px',
        boxShadow: 'none'
    },
    menuButton: {
        color:'white',
        borderRadius:0,
        backgroundColor:"#69BDEE",
        marginLeft:0,
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
          display: 'none',
        },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
        backgroundColor:'#393939'
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
}));

function DrawerNav(props) {
  const { container } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);

  function handleDrawerToggle() {
    setMobileOpen(!mobileOpen);
  }

  const drawer = (
    <div>
      <div className='drawerHeader' > 
        <div className='drawerHeaderText'><span className='orange'> -  </span><span className='blue'>BOARD</span> </div> 
      </div> 
      <List >
          <ListItem button key={'DASHBOARD'}>
            <ListItemIcon style={{ color: '#81D0FD' }}><BarChart/></ListItemIcon>
            <span className='sideBarItemText'>DASHBOARD</span> 
          </ListItem>
      </List>
      <List>
          <ListItem button key={'FORECAST'}>
            <ListItemIcon style={{ color: '#81D0FD' }} ><ShareOutlined/></ListItemIcon>
            <span className='sideBarItemText'>FORECAST</span> 
          </ListItem>
      </List>
      <List>
          <ListItem button key={'PERFORMANCE'}>
            <ListItemIcon style={{ color: '#81D0FD' }}><DonutLarge/></ListItemIcon>
            <span className='sideBarItemText'>PERFORMANCE</span> 
          </ListItem>
      </List>
      <List>
          <ListItem button key={'FILTERS'}>
            <ListItemIcon style={{ color: '#81D0FD' }}><FolderOpenOutlined/></ListItemIcon>
            <span className='sideBarItemText'>FILTERS</span> 
          </ListItem>
      </List>
      <List>
          <ListItem button key={'REPORTING'}>
            <ListItemIcon style={{ color: '#81D0FD' }}><ChromeReaderMode/></ListItemIcon>
            <span className='sideBarItemText'>REPORTING</span> 
          </ListItem>
      </List>
 
    </div>
  );

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar
            className="tollBarHeader"
        >
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
          </Typography>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
    </div>
  );
}


export default  DrawerNav;
