import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';

const axios = require('axios');

//super agent comparison
const superagent = superagentPromise(_superagent, global.Promise);


const API_ROOT = 'https://046szms56e.execute-api.us-east-2.amazonaws.com/dev/'
//const API_ROOT = 'https://lx1u658bk7.execute-api.us-east-2.amazonaws.com/default/';

const handleErrors = err => {
    if (err && err.response && err.response.status === 401) {
        console.log(err.response)
        alert('Service is unavailable')
    }
    return err;
};


const responseBody = res => res.body;


const axios_requests = {
    get: url => 
        axios 
            .get(`${API_ROOT}${url}`)
            .then(responseBody)
            .catch( (error)=> {
                handleErrors(error);
            })
};

const sa_requests = {
    get: url =>
        superagent
            .get(`${API_ROOT}${url}`)
            .set('Accept', 'application/json')
            .end(handleErrors)
            .then(responseBody)
};


const UFO = {
    getCities: () => {
        return sa_requests.get(`findCities`)
    },
    getShapes: () => {
        return sa_requests.get(`findShapes`)
    }
}

export default {
   UFO
};



