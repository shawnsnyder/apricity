import { observable, action,  runInAction, decorate } from 'mobx';
import agent from '../../api/Agent';

class UfoCityStore{
    inProgress = false
    cities = [] 
    getCities() {
        this.inProgress = true;
        return agent.UFO.getCities()
        .then( (ufo_cities)  => {
            runInAction( () => {
                this.cities.replace(ufo_cities)
            })
          })
		.catch(action((err) => {
			alert(err)
			//TODO error handling
			//TODO successfull errors too
			throw err;
		}))
        .finally(action(() => { this.inProgress  = false; }))
    }
}

decorate( UfoCityStore , {
    cities: observable,
    inProgress: observable,
    getCities: action
})

export default new UfoCityStore();
