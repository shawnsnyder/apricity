import { observable, action,  runInAction, decorate } from 'mobx';
import agent from '../../api/Agent';

class UfoShapeStore{
    inProgress = false
    shapes = [] 
    getShapes() {
        this.inProgress = true;
        return agent.UFO.getShapes()
        .then( (ufo_shapes)  => {
            runInAction( () => {
                this.shapes.replace(ufo_shapes)
            })
          })
		.catch(action((err) => {
			alert(err)
			//TODO error handling
			//TODO successfull errors too
			throw err;
		}))
        .finally(action(() => { this.inProgress  = false; }))
    }
}

decorate( UfoShapeStore , {
    shapes : observable,
    inProgress: observable,
    getShapes: action
})

export default new UfoShapeStore();
