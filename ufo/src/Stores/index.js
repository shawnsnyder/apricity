import UfoCityStore from './UfoCityStore';
import UfoShapeStore from './UfoShapeStore';

export {
    UfoCityStore,
    UfoShapeStore
}
