const mongoose = require('mongoose');
const validator = require('validator');

const model = mongoose.model('nuforc_reports', {
  city: {
    type: String,
    required: true,
    validate: {
      validator(name) {
        return validator.isAlphanumeric(name);
      },
    },
  },
  state: {
    type: String,
    required: true,
    validate: {
      validator(firstname) {
        return validator.isAlphanumeric(firstname);
      },
    },
  },
  date_time: {
    type: Date,
    required: true,
  },
  shape: {
    type: String,
    required: true,
    validate: {
      validator(city) {
        return validator.isAlphanumeric(city);
      },
    },
  }
});

module.exports = model;
