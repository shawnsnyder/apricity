'use strict';

const mongoose = require('mongoose');
const Promise = require('bluebird');
const validator = require('validator');
const nuforc_reportsModel = require('./model/nuforc_reports.js');


const MONGODB_URI = 'mongodb://ec2-user@ec2-18-221-206-137.us-east-2.compute.amazonaws.com:27017/ufo'; // or Atlas connection string

//come back to this, make cachedDB
//let cachedDb = null;

mongoose.Promise = Promise;

const createErrorResponse = (statusCode, message) => ({
  statusCode: statusCode || 501,
  headers: { 'Content-Type': 'text/plain' },
  body: message || 'Incorrect id',
});

const dbExecute = (db, fn) => db.then(fn).finally(() => mongoose.connection.close());

function dbConnectAndExecute(dbUrl, fn) {
  return dbExecute(mongoose.connect(dbUrl), fn);
}


module.exports.findCities = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    dbConnectAndExecute(MONGODB_URI , () => (
    nuforc_reportsModel  
      .aggregate([
            {
                $group: {"_id":"$city" , "number":{$sum:1}}
            },
            {
                $sort: {
                    number: -1
                }
            },
            { 
                $limit : 5 
            }
      ])
      .then( (cities) => {
               console.log(cities) 
              callback(null, { statusCode: 200, headers: { 'Access-Control-Allow-Origin': '*',  'Access-Control-Allow-Credentials': true},    body: JSON.stringify(cities) })
          })
        .finally( ()=> {
           console.log('done') 
        })
      .catch(err => callback(null, createErrorResponse(err.statusCode, err.message)))
  ));
}


module.exports.findShapes = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    dbConnectAndExecute(MONGODB_URI , () => (
    nuforc_reportsModel  
      .aggregate([
            {
                $group: {"_id":"$shape" , "number":{$sum:1}}
            },
            {
                $sort: {
                    number: -1
                }
            },
            { 
                $limit : 5 
            }
      ])
      .then( (cities) => {
               console.log(cities) 
              callback(null, { statusCode: 200, headers: { 'Access-Control-Allow-Origin': '*',  'Access-Control-Allow-Credentials': true},    body: JSON.stringify(cities) })
          })
        .finally( ()=> {
           console.log('done') 
        })
      .catch(err => callback(null, createErrorResponse(err.statusCode, err.message)))
  ));
}
